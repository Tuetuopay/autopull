from flask import Flask, request
from netaddr import IPNetwork, IPAddress
import json, os

app = Flask (__name__)

repos = None
ipWhitelist = [IPNetwork (net) for net in ["131.103.20.160/27", "165.254.145.0/26", "104.192.143.0/24", "127.0.0.0/24"]]

def pullOnPath (path):
    os.system ("cd {} && git pull".format (path))

def reloadConfig ():
    global repos
    # Loading the repo data
    with open("repos.json") as f:
        repos = json.loads (f.read ())
        f.close ()

@app.route ("/push", methods=["POST"])
def onPush ():
    # Checking the request actually comes from BitBucket
    clientIP = IPAddress (request.remote_addr)
    good = False
    for net in ipWhitelist:
        if clientIP in net: good = True
    if not good: return "Unauthorized"

    repoName, userName = request.form['repository'], request.form['actor']
    # Is the repo configured ?
    if repoName in repos:
        pullOnPath (repos[repoName])
        return "Pull complete"

    return "The specified repo does not exists"

@app.route ("/reload")
def onReload ():
    reloadConfig ()
    return "Configuration reloaded"

if __name__ == "__main__":
    reloadConfig ()
    app.run ()
